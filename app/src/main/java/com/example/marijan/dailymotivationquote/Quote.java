package com.example.marijan.dailymotivationquote;

/**
 * Created by Marijan on 27/08/2017.
 */

public class Quote
{
    public int _id;
    public String content;
    public String author;
    public boolean seen;

    Quote(String contentData, String authorData)
    {
        _id = -1;
        content = contentData;
        author = authorData;
        seen = false;
    }
}
