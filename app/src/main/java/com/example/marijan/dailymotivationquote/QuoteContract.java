package com.example.marijan.dailymotivationquote;

import android.provider.BaseColumns;

import static com.example.marijan.dailymotivationquote.QuoteContract.QuoteEntry.COLUMN_NAME_CONTENT;
import static com.example.marijan.dailymotivationquote.QuoteContract.QuoteEntry.COLUMN_NAME_SEEN;
import static com.example.marijan.dailymotivationquote.QuoteContract.QuoteEntry.TABLE_NAME;
import static com.example.marijan.dailymotivationquote.QuoteContract.QuoteEntry.COLUMN_NAME_AUTHOR;

/**
 * Created by Marijan on 27/08/2017.
 */

public final class QuoteContract
{
    private  QuoteContract(){}

    public static class QuoteEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "quotes";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_AUTHOR = "author";
        public static final String COLUMN_NAME_SEEN = "seen";
    }

    public static String TABLE_COLUMNS = "_ID INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_NAME_CONTENT + " TEXT," + COLUMN_NAME_AUTHOR + " TINYTEXT," + COLUMN_NAME_SEEN + " BOOL DEFAULT '0'";
    public static String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + "(" + TABLE_COLUMNS + ");";
    public static String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
