package com.example.marijan.dailymotivationquote;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    public TextView textViewContent;
    public TextView textViewAuthor;
    public List<Quote> quotes = new ArrayList<>();
    public int quoteSize;
    public int currentQuote = 0;
    public int currentDbVersion = 0;
    public databaseHelper dbHelper;
    public SQLiteDatabase db;
    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        dbHelper = new databaseHelper(getApplicationContext());
        db = dbHelper.getReadableDatabase();
        sharedPref = getPreferences(MODE_PRIVATE);
        editor = sharedPref.edit();

        editor.putInt("DbVersion",dbHelper.DATABASE_VERSION);
        editor.commit();

        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        textViewContent = (TextView) findViewById(R.id.quoteContent);
        textViewAuthor = (TextView) findViewById(R.id.quoteAuthor);

        String[] projection = {
                QuoteContract.QuoteEntry.COLUMN_NAME_CONTENT,
                QuoteContract.QuoteEntry.COLUMN_NAME_AUTHOR
        };

        Cursor cursor = db.query(QuoteContract.QuoteEntry.TABLE_NAME,projection,null,null,null,null,null);

        quoteSize = cursor.getCount();

        while(cursor.moveToNext())
        {

            int itemId = cursor.getColumnIndexOrThrow(QuoteContract.QuoteEntry.COLUMN_NAME_AUTHOR);
            String author = cursor.getString(itemId);
            itemId = cursor.getColumnIndexOrThrow(QuoteContract.QuoteEntry.COLUMN_NAME_CONTENT);
            String content = cursor.getString(itemId);

            quotes.add(new Quote(content,author));
        }

        cursor.close();
        SetQuote(0);
    }

    @Override
    @TargetApi(24)
    protected void onStart()
    {
        super.onStart();

        currentDbVersion = sharedPref.getInt("DbVersion",0);
        dbHelper.onUpgrade(db,currentDbVersion,0);
        editor.putInt("DbVersion",dbHelper.DATABASE_VERSION);
        editor.commit();

        Context context = getApplicationContext();
        Intent notifyIntent = new Intent(this,AlarmReceiver.class);
        notifyIntent.putExtra("QUOTE", quotes.get(currentQuote).content + " - " + quotes.get(currentQuote).author);
        selectNextQuote();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 123, notifyIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 01);
        calendar.set(Calendar.SECOND, 00);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public void SetQuote(int x)
    {
        textViewContent.setText(quotes.get(x).content);
        textViewAuthor.setText(quotes.get(x).author);
    }

    public void selectNextQuote ()
    {
        currentQuote++;
        if(currentQuote == quoteSize)
            currentQuote = 0;
    }

    public void onNextQuoteClick(View view)
    {
        selectNextQuote();
        SetQuote(currentQuote);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public void SendNotification(View view)
    {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_access_alarm)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!");

        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }
}
