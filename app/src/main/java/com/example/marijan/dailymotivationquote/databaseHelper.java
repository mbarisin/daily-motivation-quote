package com.example.marijan.dailymotivationquote;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import static com.example.marijan.dailymotivationquote.QuoteContract.*;
import static com.example.marijan.dailymotivationquote.QuoteContract.QuoteEntry.*;

/**
 * Created by Marijan on 26/08/2017.
 */

public class databaseHelper extends SQLiteOpenHelper
{
    public static int DATABASE_VERSION = 1;
    public static String DATABASE_NAME = "quote_db";
    public Quote[] firstData = {
            new Quote("The way to get started is to quit talking and begin doing.","Walt Disney"),
            new Quote("The pessimist sees difficulty in every opportunity. The optimist sees the opportunity in every difficulty.","Winston Churchill"),
            new Quote("You learn more from failure than from success. Don’t let it stop you. Failure builds character.","Unknown"),
            new Quote("It’s not whether you get knocked down, It’s whether you get up.","Vince Lombardi"),
            new Quote("If you are working on something that you really care about, you don’t have to be pushed. The vision pulls you.","Steve Jobs"),
            new Quote("Life is 10% what happens to us and 90% how we react to it.","Dennis P. Kimbro")
    };
    public int n = firstData.length;

    //constructor
    public databaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        insertValues(db,firstData,n);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // https://developer.android.com/training/basics/data-storage/databases.html#DbHelper
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        if(oldVersion != DATABASE_VERSION)
        {
            db.execSQL(TABLE_DROP);
            onCreate(db);
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void insertValues (SQLiteDatabase db,Quote[] data, int size)
    {
        for(int i = 0; i < size; i++)
        {
            //SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_CONTENT, data[i].content);
            values.put(COLUMN_NAME_AUTHOR,data[i].author);
            values.put(COLUMN_NAME_SEEN,data[i].seen);

            long newRowId = db.insert(TABLE_NAME,null,values);
        }


    }
}
