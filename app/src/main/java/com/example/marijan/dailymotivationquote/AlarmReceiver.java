package com.example.marijan.dailymotivationquote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by Marijan on 19/09/2017.
 */

public class AlarmReceiver extends BroadcastReceiver
{
    public AlarmReceiver()
    {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String extra = intent.getStringExtra("QUOTE");
        Intent intent1 = new Intent(context, NotifyService.class);
        intent1.putExtra("QUOTE", extra);
        context.startService(intent1);
    }
}
