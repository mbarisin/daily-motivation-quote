package com.example.marijan.dailymotivationquote;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import static android.content.Intent.getIntent;

/**
 * Created by Marijan on 19/09/2017.
 */

public class NotifyService extends IntentService
{
    public NotifyService() {
        super("NotifyService");
    }

    @Override
    public void onHandleIntent(Intent intent)
    {
        //String quote = intent.getStringExtra(QUOTE);
        String quote = intent.getStringExtra("QUOTE");

        Intent notifyIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 2, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_access_alarm)
                        .setContentTitle("Daily Motivation Quote")
                        .setContentText(quote)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(quote))
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setDefaults(Notification.DEFAULT_VIBRATE);

        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
